package com.jj.exoplayerseekbartest;

import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.ui.PlayerView;

import com.jj.exoplayerseekbartest.MediaSourceFactory;

public class MainActivity extends AppCompatActivity {

    private static final String URL = "http://125.133.65.197/cUV_waCkBgRpAqcZP9jSwA/1546519932/esports_battle_main/main_stream_720p/playlist.m3u8";


    DefaultTimeBar timeBar;

    PlayerView exoPlayerView;
    ExoPlayer exoPlayer;

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        exoPlayerView = findViewById(R.id.player_view);


        timeBar = findViewById(R.id.timebar);

        loadUrl(URL);

        handler.postDelayed(runnable, 1000);

    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (exoPlayer != null) {
                long buffer = exoPlayer.getBufferedPosition();
                long curretn = exoPlayer.getCurrentPosition();
                long duration = exoPlayer.getDuration();

                if (timeBar != null) {
                    timeBar.setDuration(duration);
                    timeBar.setPosition(curretn);
                    timeBar.setBufferedPosition(buffer);
                }
            }

            handler.postDelayed(this, 1000);
        }
    };

    public void loadUrl(String url) {
        Uri uri = Uri.parse(url);
        MediaSource mediaSource = MediaSourceFactory.createMediaSource(this, uri);

        exoPlayer = ExoPlayerFactory.newSimpleInstance(this);
        exoPlayerView.setPlayer(exoPlayer);
        exoPlayerView.setUseController(false);


        exoPlayer.setPlayWhenReady(true);
        exoPlayer.prepare(mediaSource);
    }
}
